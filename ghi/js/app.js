function createCard(name, description, pictureUrl, starts, ends, conventionCenter) {
    return `


        <div class="col">
                <div class="card text-center mb-3 ml-3 mr-3 shadow p-3 mb-5 rounded" style="width: 25rem;" >
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle text-muted">${conventionCenter}</h6>
                        <p class="card-text">${description}</p>
                    </div>
                    <h5 class="card-footer text-muted">
                    ${starts} - ${ends}
                    </div>
                </div>
            </div>

    </div>

    `
  }


function alert(){
    return `
    <div class="alert alert-danger" role="alert">
        Uh-oh, something broke!
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            var alertList = document.querySelector('.alert')
            alertList.innerHTML += alert()

        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startsDate = new Date(details.conference.starts)
                    const endsDate = new Date(details.conference.ends)
                    const starts = startsDate.toDateString()
                    const ends = endsDate.toDateString()
                    const conventionCenter = details.conference.location.name


                    const html = createCard(name, description, pictureUrl, starts, ends, conventionCenter);

                    const column = document.querySelector('.row');
                    column.innerHTML += html;

                }
            }

        }
    } catch (e) {
      console.error(e)
    }

});
